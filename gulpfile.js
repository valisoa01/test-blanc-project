const {src, dest} = require('gulp');
const uglify = require('gulp-uglify');
const minify = require('gulp-clean-css');
const concat = require('gulp-concat');

const pathsUserPhoto = {
    scripts: {
        src: ['./user-photo/src/scripts/*.js'],
        dest: 'build/javascripts'
    },
    stylesheets: {
        src: ['./user-photo/src/stylesheets/*.css'],
        dest: 'build/css'
    }
}

const pathsUserSelect = {
    scripts: {
        src: ['./user-select/src/scripts/*.js'],
        dest: 'build/javascripts'
    },
    stylesheets: {
        src: ['./user-select/src/stylesheets/*.css'],
        dest: 'build/css'
    }
}

//fonction pour fusionner les fichiers JS de l'utilitaire USER_PHOTO
const fusionJsUserPhoto = () => {
    return src(pathsUserPhoto.scripts.src)
        .pipe(concat('UserPhoto.js'))
        .pipe(uglify())
        .pipe(dest(pathsUserPhoto.scripts.dest))
}

//fonction pour fusionner les fichiers CSS de l'utilitaire USER_PHOTO
const fusionCssUserPhoto = () => {
    return src(pathsUserPhoto.stylesheets.src)
        .pipe(concat('UserPhoto.css'))
        .pipe(minify())
        .pipe(dest(pathsUserPhoto.stylesheets.dest))
}

//fonction pour fusioner les fichiers JS de l'utilitaire USER_SELECT
const fusionJsUserSelect = () => {
    return src(pathsUserSelect.scripts.src)
        .pipe(concat('UserSelect.js'))
        .pipe(uglify())
        .pipe(dest(pathsUserSelect.scripts.dest))
}

//fonction pour fusioner les fichiers CSS de l'utilitaire USER_SELECT
const fusionCssUserSelect = () => {
    return src(pathsUserSelect.stylesheets.src)
        .pipe(concat('UserSelect.css'))
        .pipe(minify())
        .pipe(dest(pathsUserSelect.stylesheets.dest))
}

exports.fusionJsUserPhoto = fusionJsUserPhoto;
exports.fusionCssUserPhoto = fusionCssUserPhoto;

exports.fusionJsUserSelect = fusionJsUserSelect;
exports.fusionCssUserSelect = fusionCssUserSelect;