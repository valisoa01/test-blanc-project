class UserSelectpopup extends HTMLElement {

  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
    this.UserSelectDropdown();

    window.addEventListener('load', () => {
      UserSelectDropdown(window.UserSelectDropdownOptions);
    });
  }

  diconnectedCallback() { }

  render() {
    this.innerHTML = `
        <div style="position: fixed; top: 0;right: 30%; z-index:99; display:block;" class="UserSelectPopup" id="ok">
            <div style="z-index:99;background-clip: padding-box; border-radius: 6px; border: 1px solid #999; background-color: #fff">
                <div style="border-bottom:1px solid #0d0c0c; padding-left:5px;" id="">
                    <h2>Choisir des utilisateurs :</h2>
                </div>

                <select name="myData" class="myData" multiple multiselect-search="true" tout-selectionner="true" multiselect-max-items="10" id="myData">
                    <option>LISTE PAR DEFAUT EN CAS D'ERREUR SUR L'API</option>
                    <option>John Doe</option>
                    <option>Mary Peterson</option>
                    <option>George Hansen</option>
                    <option>Mary Lauren</option>
                    <option>Garry Patrick</option>
                    <option>Andersen Anna</option>
                    <option>Victoria Blaise</option>
                    <option>Aedolf</option>
                    <option>Larry Moore</option>
                    <option>Jack Jack</option>
                    <option>Bill Georges</option>
                    <option>John Doe</option>
                    <option>Mary Peterson</option>
                    <option>George Hansen</option>
                    <option>Mary Lauren</option>
                    <option>Garry Patrick</option>
                    <option>Andersen Anna</option>
                    <option>Victoria Blaise</option>
                    <option>Aedolf</option>
                    <option>Larry Moore</option>
                    <option>Jack Jack</option>
                    <option>Bill Georges</option>
                </select>
            </div>
        </div>
        `;
  }

  UserSelectDropdown(options) {
    var parametre = {
      recherche: true,
      height: '20em',
      placeholder: 'Cliquer pour sélectionner',
      totalSelected: 'utilisateurs sélectionnés',
      texteTout: 'Tout',
      texteSuppression: 'Enlever dans la sélection',
      texteRecherche: 'Rechercher...',
      ...options
    };

    function nouveau(balise, attributs) {
      var elmnt = document.createElement(balise);
      if (attributs !== undefined) Object.keys(attributs).forEach(i => {
        if (i === 'class') {
          Array.isArray(attributs[i]) ? attributs[i].forEach(j => j !== '' ? elmnt.classList.add(j) : 0) : (attributs[i] !== '' ? elmnt.classList.add(attributs[i]) : 0)
        }
        else if (i === 'style') {
          Object.keys(attributs[i]).forEach(k => {
            elmnt.style[k] = attributs[i][k];
          });
        }
        else if (i === 'text') {
          attributs[i] === '' ? elmnt.innerHTML = '&nbsp;' : elmnt.innerText = attributs[i]
        }
        else elmnt[i] = attributs[i];
      });
      return elmnt;
    }


    document.querySelectorAll("select[multiple]").forEach((elmnt, k) => {

      var div = nouveau('div', { class: 'user-select-dropdown', style: { width: parametre.style?.width ?? elmnt.clientWidth + 'px', padding: parametre.style?.padding ?? '' } });
      elmnt.style.display = 'none';
      elmnt.parentNode.insertBefore(div, elmnt.nextSibling);
      var listWrap = nouveau('div', { class: 'user-select-dropdown-list-wrapper' });
      var list = nouveau('div', { class: 'user-select-dropdown-list', style: { height: parametre.height } });
      var recherche = nouveau('input', { class: ['user-select-dropdown-search'].concat([parametre.searchInput?.class ?? 'form-control']), style: { width: '100%', display: elmnt.attributes['multiselect-search']?.value === 'true' ? 'block' : 'none' }, placeholder: parametre.texteRecherche });
      listWrap.appendChild(recherche);
      div.appendChild(listWrap);
      listWrap.appendChild(list);

      elmnt.chargement = () => {
        list.innerHTML = '';

        if (elmnt.attributes['tout-selectionner']?.value == 'true') {
          var op = nouveau('div', { class: 'user-select-dropdown-all-selector' })
          var ic = nouveau('input', { type: 'checkbox' });
          op.appendChild(ic);
          op.appendChild(nouveau('label', { text: parametre.texteTout }));

          op.addEventListener('click', () => {
            op.classList.toggle('checked');
            op.querySelector("input").checked = !op.querySelector("input").checked;

            var ch = op.querySelector("input").checked;
            list.querySelectorAll(":scope > div:not(.user-select-dropdown-all-selector)")
              .forEach(i => { if (i.style.display !== 'none') { i.querySelector("input").checked = ch; i.optEl.selected = ch } });

            elmnt.dispatchEvent(new Event('change'));
          });
          ic.addEventListener('click', (ev) => {
            ic.checked = !ic.checked;
          });

          list.appendChild(op);
        }

        Array.from(elmnt.options).map(o => {
          var op = nouveau('div', { class: o.selected ? 'checked' : '', optEl: o })
          var ic = nouveau('input', { type: 'checkbox', checked: o.selected });
          op.appendChild(ic);
          op.appendChild(nouveau('label', { text: o.text }));

          op.addEventListener('click', () => {
            op.classList.toggle('checked');
            op.querySelector("input").checked = !op.querySelector("input").checked;
            op.optEl.selected = !!!op.optEl.selected;
            elmnt.dispatchEvent(new Event('change'));
          });
          ic.addEventListener('click', (ev) => {
            ic.checked = !ic.checked;
          });
          o.listitemEl = op;
          list.appendChild(op);
        });
        div.listEl = listWrap;

        div.refresh = () => {
          div.querySelectorAll('span.user, span.placeholder').forEach(t => div.removeChild(t));
          var sels = Array.from(elmnt.selectedOptions);
          if (sels.length > (elmnt.attributes['multiselect-max-items']?.value ?? 5)) {
            div.appendChild(nouveau('span', { class: ['user', 'selectmax'], text: sels.length + ' ' + parametre.totalSelected }));
          }
          else {
            sels.map(x => {
              var c = nouveau('span', { class: 'user', text: x.text, srcOption: x });
              if ((elmnt.attributes['multiselect-hide-x']?.value !== 'true'))
                c.appendChild(nouveau('span', { class: 'suppr', text: '🗙', title: parametre.texteSuppression, onclick: (ev) => { c.srcOption.listitemEl.dispatchEvent(new Event('click')); div.refresh(); ev.stopPropagation(); } }));

              div.appendChild(c);
            });
          }
          if (0 == elmnt.selectedOptions.length) div.appendChild(nouveau('span', { class: 'placeholder', text: elmnt.attributes['placeholder']?.value ?? parametre.placeholder }));
        };
        div.refresh();
      }
      elmnt.chargement();

      // fetch("users.json").then(d=>d.json()).then(d=>{
      //   myData.innerHTML = 
      //     d.map(t=>'<option value="'+t.value+'">'+t.text+'</option>');
      //   myData.chargement();
      // })

      //*********************************************************************** fetching API ***********************************************************************/
      const url = 'http://pp-api-licence-client.manao.eu/v1/getAllUser/000001';
      async function getUtilisateurs() {

        let api = await fetch(url, {
          method: "GET"
        }).then((result) => result.json());
        //console.log(api.data);
        var i = api.data;
        //console.log(i);
        myData.innerHTML = i.map(t => '<option value="' + t.util_id + '">' + t.util_nom + ' ' + t.util_prenom + '</option>');
        myData.chargement();

      }
      getUtilisateurs();
      //*********************************************************************** fetching API ***********************************************************************/

      recherche.addEventListener('input', () => {
        list.querySelectorAll(":scope div:not(.user-select-dropdown-all-selector)").forEach(d => {
          var txt = d.querySelector("label").innerText.toUpperCase();
          d.style.display = txt.includes(recherche.value.toUpperCase()) ? 'block' : 'none';
        });
      });

      div.addEventListener('click', () => {
        div.listEl.style.display = 'block';
        recherche.focus();
        recherche.select();
      });

      document.addEventListener('click', function (event) {
        if (!div.contains(event.target)) {
          listWrap.style.display = 'none';
          div.refresh();
        }
      });
    });
  }
}

if ('customElements' in window) {
  customElements.define('user-select-popup', UserSelectpopup);
}