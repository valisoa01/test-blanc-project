class UserSelect extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        this.addEventListener('click', this.onClick);
        this.render();
    }

    disconnectedCallback() { }

    render() {
        this.innerHTML = `

        `;
    }

    onClick() {
        let element = document.createElement('user-select-popup');
        document.body.append(element);
    }
}

if('customElements' in window) {
    customElements.define('user-select', UserSelect);
}