class UserPhoto extends HTMLElement {

    constructor() {
        super();

        this.url = "";
        this.userID = "";
        this.userFirstName = "";
        this.userLastName = "";
        this.userPrenomUsage = "";
        this.userMail = "";
        this.infosParamName = "";
        this.infosParamPhoto = "";
    }

    connectedCallback() {
        //if (!this.getAttribute("u-url")) return console.error("URL is not specified");

        if (!this.getAttribute("u-id"))
            return console.error("User ID is not specified");

        this.getData();

        this.addEventListener("click", this.onClick);

        //this.getElementsByTagName("user-photo").style.display = "inline-block";

        this.render();
    }

    disconnectedCallback() { }

    render() {

        if (!this.url) {
            if (!this.userPrenomUsage) {
                this.userPrenomUsage = this.userLastName;
            }
            this.innerHTML = `
            <div style="border-radius:50%; width:32px; height:32px; display:table;" title="${this.userFirstName} ${this.userLastName}">
                <span style="text-align:center; border-radius:50%; vertical-align:middle; display:table-cell;  background-color:#002223; color:white; font-size:18px; cursor:pointer">${this.userPrenomUsage[0]}</span> 
            </div>
        `;
        } else {
            if (!this.userPrenomUsage) {
                this.userPrenomUsage = this.userLastName;
            }
            this.innerHTML = `
            <div style="display:inline-block;" title="${this.userFirstName} ${this.userLastName}">
                <img style="border-radius:50%; width:32px; cursor:pointer" src='${this.url}' /> 
            </div>
        `;
        }
    }

    getData() {
        this.url = this.getAttribute("u-url");
        this.userID = this.getAttribute("u-id");
        this.userFirstName = this.getAttribute("u-firstname");
        this.userLastName = this.getAttribute("u-lastname");
        this.userPrenomUsage = this.getAttribute("u-prenomusage");
        this.userMail = this.getAttribute("u-mail");
        this.infosParamName = this.getAttribute("u-param-infos-name");
        this.infosParamPhoto = this.getAttribute("u-param-infos-photo");
    }

    setData(element) {
        element.setAttribute('id', "ui-modal-" + this.userID);
        element.setAttribute("u-url", this.url);
        element.setAttribute("u-id", this.userID);
        element.setAttribute("u-firstname", this.userFirstName);
        element.setAttribute("u-lastname", this.userLastName);
        element.setAttribute("u-prenomusage", this.userPrenomUsage);
        element.setAttribute("u-mail", this.userMail);
        element.setAttribute("u-param-infos-name", this.infosParamName);
        element.setAttribute("u-param-infos-photo", this.infosParamPhoto);
    }

    onClick() {

        // delete all popup
        this.dismissAllPopupInformation();

        // create new popup
        let element = document.createElement("user-infos-popup");
        this.setData(element);
        document.body.append(element);

    }

    dismissAllPopupInformation() {
        let elements = document.getElementsByTagName("user-infos-popup");
        if (elements.length) {
            for (let i = 0; i < elements.length; i++) elements[i].remove();
        }
    }
}

if ('customElements' in window) {
    customElements.define('user-photo', UserPhoto);
}
