class PopupUserInformations extends HTMLElement {

    constructor() {
        super();

        this.url = "";
        this.userID = "";
        this.userFirstName = "";
        this.userLastName = "";
        this.userPrenomUsage = "";
        this.userMail = "";
        this.infosParamName = "";
        this.infosParamPhoto = "";
    }

    connectedCallback() {

        this.getData();

        let elementID = this.getAttribute("id");

        this.render();

        document.getElementById("dismiss-ui-modal-" + this.userID).addEventListener("click", () => {
            let element = document.getElementById(elementID);
            element.remove();
        });
    }

    disconnectedCallback() { }

    getData() {
        
        this.url = this.getAttribute("u-url");
        this.userID = this.getAttribute("u-id");
        this.userFirstName = this.getAttribute("u-firstname");
        this.userLastName = this.getAttribute("u-lastname");
        this.userPrenomUsage = this.getAttribute("u-prenomusage");
        this.userMail = this.getAttribute("u-mail");
        this.infosParamName = this.getAttribute("u-param-infos-name");
        this.infosParamPhoto = this.getAttribute("u-param-infos-photo");
    }

    render() {

        if (!this.url) {
            this.innerHTML = `
                <div style="position: fixed; top: 0;right: 40%; z-index:100;">
                    <div style="z-index:99; margin: 80px auto; width:400px; background-clip: padding-box; border-radius: 6px; border: 1px solid #999;background-color: #fff">
                        <div style="font-weight:700; border-bottom:1px solid #0d0c0c;">
                            <h2 style="text-align:center">Informations</h2>
                        </div>
                    <div style="font-weight:700; border-bottom:1px solid #0d0c0c; padding-top:10px;">
                        <div style="text-align:center; background-color:#002223; width:150px; height: 150px; border-radius:50%; display: table; margin: 0 auto;">
                            <p style="text-align:center; vertical-align:middle; display:table-cell; color:white; font-size:50px;">${this.userLastName[0]}</p>
                        </div>
                        <div>
                            <p style="text-align:center; font-weight:700;">${this.userFirstName} ${this.userLastName} (${this.userPrenomUsage})</p>
                            <p style="text-align:center;">${this.userMail}</p>
                        </div>
                    </div>
                    <div style="text-align:right; padding:10px 20px">
                        <button id="dismiss-ui-modal-${this.userID}" style="padding: 10px 16px;font-size:15px;border-radius: 6px; border:1px solid #e3e3e3; cursor:pointer;">Fermer</button>
                    </div>
                </div>
        `;
        }else {
            this.innerHTML = `
                <div style="position: fixed; top: 0;right: 40%; z-index:100;">
                    <div style="z-index:99; margin: 80px auto; width:400px; background-clip: padding-box; border-radius: 6px; border: 1px solid #999; background-color: #fff">
                        <div style="font-weight:700; border-bottom:1px solid #0d0c0c;">
                            <h2 style="text-align:center">Informations</h2>
                        </div>
                        <div style="font-weight:700; border-bottom:1px solid #0d0c0c; padding-top:10px;">
                            <div style="text-align:center">
                                <img style="width:150px;" src='${this.url}' />
                            </div>
                            <div>
                                <p style="text-align:center; font-weight:700;">${this.userFirstName} ${this.userLastName} (${this.userPrenomUsage})</p>
                                <p style="text-align:center;">${this.userMail}</p>
                            </div>
                        </div>
                        <div style="text-align:right; padding:10px 20px">
                            <button id="dismiss-ui-modal-${this.userID}" style="padding: 10px 16px;font-size:15px;border-radius: 6px; border:1px solid #e3e3e3; cursor:pointer;">Fermer</button>
                        </div>
                    </div>
                </div>
        `;
        }
        
    }

}

if ('customElements' in window) {
    customElements.define('user-infos-popup', PopupUserInformations);
}
