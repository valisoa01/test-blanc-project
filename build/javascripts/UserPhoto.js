class PopupUserInformations extends HTMLElement{constructor(){super(),this.url="",this.userID="",this.userFirstName="",this.userLastName="",this.userPrenomUsage="",this.userMail="",this.infosParamName="",this.infosParamPhoto=""}connectedCallback(){this.getData();let t=this.getAttribute("id");this.render(),document.getElementById("dismiss-ui-modal-"+this.userID).addEventListener("click",()=>{document.getElementById(t).remove()})}disconnectedCallback(){}getData(){this.url=this.getAttribute("u-url"),this.userID=this.getAttribute("u-id"),this.userFirstName=this.getAttribute("u-firstname"),this.userLastName=this.getAttribute("u-lastname"),this.userPrenomUsage=this.getAttribute("u-prenomusage"),this.userMail=this.getAttribute("u-mail"),this.infosParamName=this.getAttribute("u-param-infos-name"),this.infosParamPhoto=this.getAttribute("u-param-infos-photo")}render(){this.url?this.innerHTML=`
                <div style="position: fixed; top: 0;right: 40%; z-index:100;">
                    <div style="z-index:99; margin: 80px auto; width:400px; background-clip: padding-box; border-radius: 6px; border: 1px solid #999; background-color: #fff">
                        <div style="font-weight:700; border-bottom:1px solid #0d0c0c;">
                            <h2 style="text-align:center">Informations</h2>
                        </div>
                        <div style="font-weight:700; border-bottom:1px solid #0d0c0c; padding-top:10px;">
                            <div style="text-align:center">
                                <img style="width:150px;" src='${this.url}' />
                            </div>
                            <div>
                                <p style="text-align:center; font-weight:700;">${this.userFirstName} ${this.userLastName} (${this.userPrenomUsage})</p>
                                <p style="text-align:center;">${this.userMail}</p>
                            </div>
                        </div>
                        <div style="text-align:right; padding:10px 20px">
                            <button id="dismiss-ui-modal-${this.userID}" style="padding: 10px 16px;font-size:15px;border-radius: 6px; border:1px solid #e3e3e3; cursor:pointer;">Fermer</button>
                        </div>
                    </div>
                </div>
        `:this.innerHTML=`
                <div style="position: fixed; top: 0;right: 40%; z-index:100;">
                    <div style="z-index:99; margin: 80px auto; width:400px; background-clip: padding-box; border-radius: 6px; border: 1px solid #999;background-color: #fff">
                        <div style="font-weight:700; border-bottom:1px solid #0d0c0c;">
                            <h2 style="text-align:center">Informations</h2>
                        </div>
                    <div style="font-weight:700; border-bottom:1px solid #0d0c0c; padding-top:10px;">
                        <div style="text-align:center; background-color:#002223; width:150px; height: 150px; border-radius:50%; display: table; margin: 0 auto;">
                            <p style="text-align:center; vertical-align:middle; display:table-cell; color:white; font-size:50px;">${this.userLastName[0]}</p>
                        </div>
                        <div>
                            <p style="text-align:center; font-weight:700;">${this.userFirstName} ${this.userLastName} (${this.userPrenomUsage})</p>
                            <p style="text-align:center;">${this.userMail}</p>
                        </div>
                    </div>
                    <div style="text-align:right; padding:10px 20px">
                        <button id="dismiss-ui-modal-${this.userID}" style="padding: 10px 16px;font-size:15px;border-radius: 6px; border:1px solid #e3e3e3; cursor:pointer;">Fermer</button>
                    </div>
                </div>
        `}}"customElements"in window&&customElements.define("user-infos-popup",PopupUserInformations);class UserPhoto extends HTMLElement{constructor(){super(),this.url="",this.userID="",this.userFirstName="",this.userLastName="",this.userPrenomUsage="",this.userMail="",this.infosParamName="",this.infosParamPhoto=""}connectedCallback(){if(!this.getAttribute("u-id"))return console.error("User ID is not specified");this.getData(),this.addEventListener("click",this.onClick),this.render()}disconnectedCallback(){}render(){this.url?(this.userPrenomUsage||(this.userPrenomUsage=this.userLastName),this.innerHTML=`
            <div style="display:inline-block;" title="${this.userFirstName} ${this.userLastName}">
                <img style="border-radius:50%; width:32px; cursor:pointer" src='${this.url}' /> 
            </div>
        `):(this.userPrenomUsage||(this.userPrenomUsage=this.userLastName),this.innerHTML=`
            <div style="border-radius:50%; width:32px; height:32px; display:table;" title="${this.userFirstName} ${this.userLastName}">
                <span style="text-align:center; border-radius:50%; vertical-align:middle; display:table-cell;  background-color:#002223; color:white; font-size:18px; cursor:pointer">${this.userPrenomUsage[0]}</span> 
            </div>
        `)}getData(){this.url=this.getAttribute("u-url"),this.userID=this.getAttribute("u-id"),this.userFirstName=this.getAttribute("u-firstname"),this.userLastName=this.getAttribute("u-lastname"),this.userPrenomUsage=this.getAttribute("u-prenomusage"),this.userMail=this.getAttribute("u-mail"),this.infosParamName=this.getAttribute("u-param-infos-name"),this.infosParamPhoto=this.getAttribute("u-param-infos-photo")}setData(t){t.setAttribute("id","ui-modal-"+this.userID),t.setAttribute("u-url",this.url),t.setAttribute("u-id",this.userID),t.setAttribute("u-firstname",this.userFirstName),t.setAttribute("u-lastname",this.userLastName),t.setAttribute("u-prenomusage",this.userPrenomUsage),t.setAttribute("u-mail",this.userMail),t.setAttribute("u-param-infos-name",this.infosParamName),t.setAttribute("u-param-infos-photo",this.infosParamPhoto)}onClick(){this.dismissAllPopupInformation();var t=document.createElement("user-infos-popup");this.setData(t),document.body.append(t)}dismissAllPopupInformation(){var e=document.getElementsByTagName("user-infos-popup");if(e.length)for(let t=0;t<e.length;t++)e[t].remove()}}"customElements"in window&&customElements.define("user-photo",UserPhoto);