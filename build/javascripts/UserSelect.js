class UserSelectpopup extends HTMLElement{constructor(){super()}connectedCallback(){this.render(),this.UserSelectDropdown(),window.addEventListener("load",()=>{UserSelectDropdown(window.UserSelectDropdownOptions)})}diconnectedCallback(){}render(){this.innerHTML=`
        <div style="position: fixed; top: 0;right: 30%; z-index:99; display:block;" class="UserSelectPopup" id="ok">
            <div style="z-index:99;background-clip: padding-box; border-radius: 6px; border: 1px solid #999; background-color: #fff">
                <div style="border-bottom:1px solid #0d0c0c; padding-left:5px;" id="">
                    <h2>Choisir des utilisateurs :</h2>
                </div>

                <select name="myData" class="myData" multiple multiselect-search="true" tout-selectionner="true" multiselect-max-items="10" id="myData">
                    <option>LISTE PAR DEFAUT EN CAS D'ERREUR SUR L'API</option>
                    <option>John Doe</option>
                    <option>Mary Peterson</option>
                    <option>George Hansen</option>
                    <option>Mary Lauren</option>
                    <option>Garry Patrick</option>
                    <option>Andersen Anna</option>
                    <option>Victoria Blaise</option>
                    <option>Aedolf</option>
                    <option>Larry Moore</option>
                    <option>Jack Jack</option>
                    <option>Bill Georges</option>
                    <option>John Doe</option>
                    <option>Mary Peterson</option>
                    <option>George Hansen</option>
                    <option>Mary Lauren</option>
                    <option>Garry Patrick</option>
                    <option>Andersen Anna</option>
                    <option>Victoria Blaise</option>
                    <option>Aedolf</option>
                    <option>Larry Moore</option>
                    <option>Jack Jack</option>
                    <option>Bill Georges</option>
                </select>
            </div>
        </div>
        `}UserSelectDropdown(e){var c={recherche:!0,height:"20em",placeholder:"Cliquer pour sélectionner",totalSelected:"utilisateurs sélectionnés",texteTout:"Tout",texteSuppression:"Enlever dans la sélection",texteRecherche:"Rechercher...",...e};function r(e,o){var n=document.createElement(e);return void 0!==o&&Object.keys(o).forEach(t=>{"class"===t?Array.isArray(o[t])?o[t].forEach(e=>""!==e?n.classList.add(e):0):""!==o[t]&&n.classList.add(o[t]):"style"===t?Object.keys(o[t]).forEach(e=>{n.style[e]=o[t][e]}):"text"===t?""===o[t]?n.innerHTML="&nbsp;":n.innerText=o[t]:n[t]=o[t]}),n}document.querySelectorAll("select[multiple]").forEach((n,e)=>{var o=r("div",{class:"user-select-dropdown",style:{width:c.style?.width??n.clientWidth+"px",padding:c.style?.padding??""}}),l=(n.style.display="none",n.parentNode.insertBefore(o,n.nextSibling),r("div",{class:"user-select-dropdown-list-wrapper"})),i=r("div",{class:"user-select-dropdown-list",style:{height:c.height}}),s=r("input",{class:["user-select-dropdown-search"].concat([c.searchInput?.class??"form-control"]),style:{width:"100%",display:"true"===n.attributes["multiselect-search"]?.value?"block":"none"},placeholder:c.texteRecherche});l.appendChild(s),o.appendChild(l),l.appendChild(i),n.chargement=()=>{var e,t;i.innerHTML="","true"==n.attributes["tout-selectionner"]?.value&&(e=r("div",{class:"user-select-dropdown-all-selector"}),t=r("input",{type:"checkbox"}),e.appendChild(t),e.appendChild(r("label",{text:c.texteTout})),e.addEventListener("click",()=>{e.classList.toggle("checked"),e.querySelector("input").checked=!e.querySelector("input").checked;var t=e.querySelector("input").checked;i.querySelectorAll(":scope > div:not(.user-select-dropdown-all-selector)").forEach(e=>{"none"!==e.style.display&&(e.querySelector("input").checked=t,e.optEl.selected=t)}),n.dispatchEvent(new Event("change"))}),t.addEventListener("click",e=>{t.checked=!t.checked}),i.appendChild(e)),Array.from(n.options).map(e=>{var t=r("div",{class:e.selected?"checked":"",optEl:e}),o=r("input",{type:"checkbox",checked:e.selected});t.appendChild(o),t.appendChild(r("label",{text:e.text})),t.addEventListener("click",()=>{t.classList.toggle("checked"),t.querySelector("input").checked=!t.querySelector("input").checked,t.optEl.selected=!t.optEl.selected,n.dispatchEvent(new Event("change"))}),o.addEventListener("click",e=>{o.checked=!o.checked}),e.listitemEl=t,i.appendChild(t)}),o.listEl=l,o.refresh=()=>{o.querySelectorAll("span.user, span.placeholder").forEach(e=>o.removeChild(e));var e=Array.from(n.selectedOptions);e.length>(n.attributes["multiselect-max-items"]?.value??5)?o.appendChild(r("span",{class:["user","selectmax"],text:e.length+" "+c.totalSelected})):e.map(e=>{var t=r("span",{class:"user",text:e.text,srcOption:e});"true"!==n.attributes["multiselect-hide-x"]?.value&&t.appendChild(r("span",{class:"suppr",text:"🗙",title:c.texteSuppression,onclick:e=>{t.srcOption.listitemEl.dispatchEvent(new Event("click")),o.refresh(),e.stopPropagation()}})),o.appendChild(t)}),0==n.selectedOptions.length&&o.appendChild(r("span",{class:"placeholder",text:n.attributes.placeholder?.value??c.placeholder}))},o.refresh()},n.chargement();!async function(){var e=(await fetch("http://pp-api-licence-client.manao.eu/v1/getAllUser/000001",{method:"GET"}).then(e=>e.json())).data;myData.innerHTML=e.map(e=>'<option value="'+e.util_id+'">'+e.util_nom+" "+e.util_prenom+"</option>"),myData.chargement()}(),s.addEventListener("input",()=>{i.querySelectorAll(":scope div:not(.user-select-dropdown-all-selector)").forEach(e=>{var t=e.querySelector("label").innerText.toUpperCase();e.style.display=t.includes(s.value.toUpperCase())?"block":"none"})}),o.addEventListener("click",()=>{o.listEl.style.display="block",s.focus(),s.select()}),document.addEventListener("click",function(e){o.contains(e.target)||(l.style.display="none",o.refresh())})})}}"customElements"in window&&customElements.define("user-select-popup",UserSelectpopup);class UserSelect extends HTMLElement{constructor(){super()}connectedCallback(){this.addEventListener("click",this.onClick),this.render()}disconnectedCallback(){}render(){this.innerHTML=`

        `}onClick(){var e=document.createElement("user-select-popup");document.body.append(e)}}"customElements"in window&&customElements.define("user-select",UserSelect);